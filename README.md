
CARTO frontend test
=

Instructions
-
- npm install
- `npm run watch` for the wepback dev server
- Open [Localhost](http://localhost:8080), or [Your local ip](http://IP:8080) if you want to show your friends and family.

There's a couple of spec.js files. You can run jest with

- `npm test` or `npm test:watch`

Note: I used npm 3, haven't tested with 2, not sure if will work.

I logged my mental process (sort of) on LOG.md