var events = require('EventBus/Events.js');
var Color = require('Color/Color.js');

function StyleManager (eventBus) {
  this.eventBus = eventBus;
  this.style = {
    baseFillColor: new Color(0, 0, 0, 1),
    fillColor: "#000",
    radius: 5,
    baseRadius: 5,
    fillOpacity: 1,
    weight: 0,
    color: new Color(0, 0, 0, 1).toString(),
    opacity: 1
  };

  this.requestUpdate = function () {
    this.eventBus.fire(events.STYLE_UPDATED);
  }

  this.eventBus.addMegaHandler(function (type, payload) {
    switch(type) {
      case events.FILL_COLOR_UPDATE:
        this.style.baseFillColor = payload;
        this.style.fillColor = this.style.baseFillColor.toString();
        break;
      case events.POINT_SIZE_UPDATE:
        if (payload === this.style.baseRadius) return;
        this.style.baseRadius = payload;
        break;
      case events.STROKE_COLOR_UPDATE:
        this.style.color = payload.toString();
        break;
      case events.STROKE_SIZE_UPDATE:
        if (payload === this.style.weight) return;
        this.style.weight = payload;
        break;
      default:
        // ignore event
        return;
    }

    this.requestUpdate();
  }, this);
}

module.exports = StyleManager;
