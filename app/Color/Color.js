function Color (r, g, b, a) {
  this.r = r;
  this.g = g;
  this.b = b;
  this.a = a === undefined ? 1 : a;

  /* http://www.rapidtables.com/convert/color/rgb-to-hsl.htm */
  this.HSL = function () {
    var cR = this.r / 255;
    var cG = this.g / 255;
    var cB = this.b / 255;

    var cMax = Math.max(cB, cR, cG);
    var cMin = Math.min(cB, cR, cG);

    var delta = cMax - cMin;
    this.l = (cMax + cMin) / 2;
    
    if (delta === 0) {
      this.s = 0;
      this.h = 0;
    } else {
      this.s = delta / (1 - Math.abs((2 * this.l) - 1));

      switch(cMax) {
        case cR:
          this.h = ((cG - cB) / delta) % 6;
          break;
        case cG:
          this.h = ((cB - cR) / delta) + 2;
          break;
        case cB:
          this.h = ((cR - cG) / delta) + 4;
          break;
      }

      this.h = Math.round(this.h * 60);
      this.s = Math.round(this.s * 100);
      this.l = Math.round(this.l * 100);
    }
  }

  this.toString = function () {
    return 'rgba(' + [this.r, this.g, this.b, this.a].join(',') + ')';
  }.bind(this);

  this.lightness = function (l) {
    return 'hsla(' + this.h + ',' + this.s + '%,' + l + "%," + this.a + ')';
  }

  this.setColor = function (r, g, b, a) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a === undefined ? 1 : a;
    this.HSL();
  }.bind(this);

  this.toAlpha = function (a) {
    return new Color(this.r, this.g, this.b, a);
  }

  this.HSL();
}

module.exports = Color;
