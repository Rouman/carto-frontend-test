var Color = require('./Color.js');

describe('Color tests', function () {
  it('should return correct rgba representation', function () {
    var color = new Color(0, 0, 0, 1);
    expect(color.toString()).toBe("rgba(0,0,0,1)");

    color = new Color(255, 0, 0, 0);
    expect(color.toString()).toBe("rgba(255,0,0,0)");

    color.setColor(128, 128, 128, 1);
    expect(color.toString()).toBe("rgba(128,128,128,1)");

    color.setColor(128, 128, 128, 0);
    expect(color.toString()).toBe("rgba(128,128,128,0)");
  });

  it('should allow to set alpha individually', function () {
    var color = new Color(0, 0, 0, 1);
    expect(color.toString()).toBe("rgba(0,0,0,1)");
    color = color.toAlpha(0.5);
    expect(color.toString()).toBe("rgba(0,0,0,0.5)");
  })
});
