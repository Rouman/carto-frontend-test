function EventBus () {

  // EVENT_NAME => {handler, context}
  this.subscriptions = {}
  this.megaSubscriptions = [];

  this.addHandler = function (type, handler, context) {
    if (this.subscriptions[type] === undefined) {
      this.subscriptions[type] = [];
    }

    if (typeof handler === 'function') {
      this.subscriptions[type].push({
        handler: handler,
        context: context
      });
    }
  }

  // This is called for ALL events, in case we prefer a big function instead of a lot of small ones
  this.addMegaHandler = function (handler, context) {
    this.megaSubscriptions.push({
      handler: handler,
      context: context
    });
  };
  
  this.fire = function (type, payload) {
    this.megaSubscriptions.forEach(function (value) {
      value.handler.call(value.context, type, payload);
    });

    if (this.subscriptions[type] !== undefined) {
      this.subscriptions[type].forEach(function (value) {
        value.handler.call(value.context, payload);
      });
    }
  };

  // TODO: implement this if necessary
  this.removeHandler = function () {};

}

module.exports = EventBus;
