var EventBus = require('./EventBus');

describe("Basic EventBus behaviour", function () {
  var eventBus;
  
  beforeEach(function () {
    eventBus = new EventBus();
  });

  test("an event is added", function () {
    eventBus.addHandler("TEST", function () {});
    expect(eventBus.subscriptions["TEST"].length).toBe(1);
  });

  test("multiple events are added", function () {
    eventBus.addHandler("TEST", function () {});
    eventBus.addHandler("TEST", function () {});
    expect(eventBus.subscriptions["TEST"].length).toBe(2);
  });

  test("an event is fired", function () {
    var mockFn = jest.fn();
    eventBus.addHandler("TEST", mockFn);
    eventBus.fire("TEST");

    expect(mockFn.mock.calls.length).toBe(1);
  });

  test("all attached events are fired", function () {
    var mockFn = jest.fn();
    var otherMock = jest.fn();
    eventBus.addHandler("TEST", mockFn);
    eventBus.addHandler("TEST", otherMock);
    eventBus.fire("TEST");

    expect(mockFn.mock.calls.length).toBe(1);
    expect(otherMock.mock.calls.length).toBe(1);
  });

  test("'this' context is kept", function () {
    var context = {

    };
    eventBus.addHandler("TEST", function () {
      this.truth = true;
    }, context);
    eventBus.fire("TEST");

    expect(context.truth).toBe(true);
  });

  describe("Mega handlers", function () {
    test("Can add a mega handler", function () {
      var handler = jest.fn();
      eventBus.addMegaHandler(handler);
      expect(eventBus.megaSubscriptions.length).toBe(1);
    });

    test("Mega Handlers are called for all events", function () {
      var handler = jest.fn();
      eventBus.addMegaHandler(handler);

      eventBus.fire("SCHEZUAN_SAUCE");
      expect(handler.mock.calls.length).toBe(1);
      
      eventBus.fire("SHOW_ME_WHAT_YOU_GOT");
      expect(handler.mock.calls.length).toBe(2);
    });

    test("Mega Handlers receive the type and payload", function () {
      var theType = "RETICULATING_SPLINES";
      var thePayload = 80;

      var handler = function (type, payload) {
        expect(type).toBe(theType);
        expect(payload).toBe(thePayload);
      };
      eventBus.addMegaHandler(handler);
      
      eventBus.fire(theType, thePayload);
    });
  });
});