require('./Drawer.css');
var FillColor = require('./Controls/FillColor.js');
var PointSize = require('./Controls/PointSize.js');
var StrokeSize = require('./Controls/StrokeSize.js');
var StrokeColor = require('./Controls/StrokeColor.js');
var FilterData = require('./Controls/FilterData.js');

function Drawer (node, eventBus) {
  this.node = node;
  this.content = this.node.querySelector(".content");
  this.close = node.querySelector(".close");
  this.filterData = new FilterData(this.content, eventBus);
  
  var styleHeader = document.createElement("h4");
  styleHeader.innerText = "Style";
  this.content.appendChild(styleHeader);

  var separator = document.createElement("hr");
  this.content.appendChild(separator);

  this.fillColor = new FillColor(this.content, eventBus);
  this.pointSize = new PointSize(this.content, eventBus);
  this.strokeColor = new StrokeColor(this.content, eventBus);
  this.strokeSize = new StrokeSize(this.content, eventBus);

  this.handleClick = function () {
    this.node.className = "open";
  }

  this.closeDrawer = function (e) {
    e.stopPropagation();
    this.node.className = "";
  }

  this.node.addEventListener("click", this.handleClick.bind(this));
  this.close.addEventListener("click", this.closeDrawer.bind(this));
}

module.exports = Drawer;
