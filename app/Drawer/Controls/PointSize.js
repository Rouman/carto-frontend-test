var events = require('EventBus/Events.js');
var Slider = require('./Slider.js');
require("./PointSize.css");

function PointSize (parent, eventBus) {
  this.container = document.createElement("div");
  this.container.className = "pointSize controlGroup";
  
  var proportionalLabel = document.createElement("label");
  proportionalLabel.className = "checkboxLabel";

  var proportionalText = document.createElement("span");
  proportionalText.innerText = "Proportional to population";

  var proportional = document.createElement("input");
  proportional.setAttribute("type", "checkbox");

  proportionalLabel.appendChild(proportional);
  proportionalLabel.appendChild(proportionalText);

  proportional.addEventListener("change", function () {
    eventBus.fire(events.SIZE_RELATIVE_POPULATION, proportional.checked);
  });
  
  var slider = new Slider(this.container, 2, 20, 5, "Point size", true);
  slider.onValueChange = function (value) {
    eventBus.fire(events.POINT_SIZE_UPDATE, parseInt(value));
  }

  this.container.appendChild(proportionalLabel);

  parent.appendChild(this.container);
}

module.exports = PointSize;
