var ColorPicker = require('./ColorPicker.js');
var events = require('EventBus/Events.js');

function FillColor (node, eventBus) {
  this.container = document.createElement("div");
  this.container.className = "controlGroup";

  var proportionalLabel = document.createElement("label");
  proportionalLabel.className = "checkboxLabel";
  
  var proportionalCaption = document.createElement("span");
  proportionalCaption.innerText = "Proportional to population";

  var proportional = document.createElement("input");
  proportional.setAttribute("type", "checkbox");

  proportionalLabel.appendChild(proportional);
  proportionalLabel.appendChild(proportionalCaption);

  proportional.addEventListener("change", function () {
    eventBus.fire(events.FILL_RELATIVE_POPULATION, proportional.checked);
  });

  this.color = new ColorPicker(this.container, "Fill color");
  this.container.appendChild(proportionalLabel);

  this.color.onValueChange = function (color) {
    eventBus.fire(events.FILL_COLOR_UPDATE, color);
  };

  node.appendChild(this.container);
}

module.exports = FillColor;
