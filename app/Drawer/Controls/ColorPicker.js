var Slider = require('./Slider.js');
var Color = require('Color/Color.js');
require('./ColorPicker.css');

function ColorPicker (node, caption) {
  this.buttonContainer = document.createElement("div");
  this.buttonContainer.className = "buttonContainer";

  var label = document.createElement("label");
  label.innerText = caption;

  this.button = document.createElement("div");
  this.button.className = "colorPickerButton";

  var colorIndicator = document.createElement("div");
  colorIndicator.className = "colorIndicator";
  this.button.appendChild(colorIndicator);

  this.buttonContainer.appendChild(label);
  this.buttonContainer.appendChild(this.button);

  this.container = document.createElement("div");
  this.container.className = "colorpicker";
  this.color = new Color(0, 0, 0, 1);
  
  this.container.addEventListener("mouseleave", function () {
    this.container.remove();
  }.bind(this));

  this.updateColor = function () {
    this.color.setColor(
      this.rSlider.getValue(),
      this.gSlider.getValue(),
      this.bSlider.getValue(),
      this.aSlider.getValue() / 255
    );

    if (this.onValueChange) {
      this.onValueChange(this.color);
    }

    colorIndicator.style.backgroundColor = this.color.toString();
  }.bind(this);

  var sliderWrapper = document.createElement("div");
  sliderWrapper.className = "sliderWrapper";
  this.container.appendChild(sliderWrapper);

  this.rSlider = new Slider(sliderWrapper, 0, 255, this.color.r, 'R');
  this.gSlider = new Slider(sliderWrapper, 0, 255, this.color.g, 'G');
  this.bSlider = new Slider(sliderWrapper, 0, 255, this.color.b, 'B');
  this.aSlider = new Slider(sliderWrapper, 0, 255, 255, 'A');

  this.rSlider.hideValue();
  this.gSlider.hideValue();
  this.bSlider.hideValue();
  this.aSlider.hideValue();

  this.rSlider.onValueChange = this.updateColor;
  this.gSlider.onValueChange = this.updateColor;
  this.bSlider.onValueChange = this.updateColor;
  this.aSlider.onValueChange = this.updateColor;

  this.button.addEventListener("click", function (e) {
    node.appendChild(this.container);
    this.container.style.left = this.button.offsetLeft;
    this.container.style.top = this.button.offsetTop;
  }.bind(this));

  node.appendChild(this.buttonContainer);
  this.updateColor();
}

module.exports = ColorPicker;
