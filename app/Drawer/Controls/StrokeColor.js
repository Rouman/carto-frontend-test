var ColorPicker = require('./ColorPicker.js');
var events = require('EventBus/Events.js');

function StrokeColor (node, eventBus) {
  this.container = document.createElement("div");
  this.container.className = "controlGroup";

  this.color = new ColorPicker(this.container, "Border color");

  this.color.onValueChange = function (color) {
    eventBus.fire(events.STROKE_COLOR_UPDATE, color);
  };

  node.appendChild(this.container);
}

module.exports = StrokeColor;
