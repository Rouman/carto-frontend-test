require('./Slider.css');
require('./range.css');

// This wraps a input type="range" into a Slider, exposing a onValueChange
function Slider (container, min, max, value, caption, flexLabel) {
  this.wrapper = document.createElement("div");
  this.wrapper.className = "slider";
  
  this.caption = document.createElement("label");
  this.caption.innerText = caption;
  
  if (flexLabel) {
    this.caption.className = "labelFlex";
  }
  
  this.value = document.createElement("span");

  this.input = document.createElement("input");
  this.input.setAttribute("type", "range");
  this.input.min = min;
  this.input.max = max;
  this.input.value = value;

  this.handleInput = function () {
    this.value.innerText = this.input.value;

    if (this.onValueChange) {
      this.onValueChange(this.input.value);
    }
  }.bind(this);

  this.getValue = function () {
    return this.input.value;
  }.bind(this);

  this.hideValue = function () {
    this.value.className = "hidden";
  }

  this.input.addEventListener("change", this.handleInput);
  this.input.addEventListener("input", this.handleInput);

  if (caption !== undefined) {
    this.wrapper.appendChild(this.caption);
  }
  
  this.wrapper.appendChild(this.input);
  this.wrapper.appendChild(this.value);

  container.appendChild(this.wrapper);
  this.handleInput();
}

module.exports = Slider;
