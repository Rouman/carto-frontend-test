var events = require('EventBus/Events.js');
require('./FilterData.css');

function FilterData (node, eventBus) {
  this.container = document.createElement("div");
  
  var title = document.createElement("h4");
  this.container.appendChild(title);
  title.innerText = "Filter data";

  var separator = document.createElement("hr");
  this.container.appendChild(separator);

  var enableLabel = document.createElement("label");
  enableLabel.className = "checkboxLabel";
  this.container.appendChild(enableLabel);
  var enableCheckboxText = document.createElement("span");
  enableCheckboxText.innerText = "Enable filters";

  var enableCheckbox = document.createElement("input");
  enableCheckbox.setAttribute("type", "checkbox");

  enableLabel.appendChild(enableCheckbox);
  enableLabel.appendChild(enableCheckboxText);

  var minLabel = document.createElement("label");
  minLabel.className = "inputLabel";
  var minLabelText = document.createElement("span");
  minLabelText.innerText = "Min population";
  var minInput = document.createElement("input");
  minInput.setAttribute("type", "number");
  minInput.setAttribute("disabled", true);
  minInput.setAttribute("min", 0);
  minInput.value = 0;

  minLabel.appendChild(minLabelText);
  minLabel.appendChild(minInput);

  this.container.appendChild(minLabel);

  var maxLabel = document.createElement("label");
  maxLabel.className = "inputLabel";
  var maxLabelText = document.createElement("span");
  maxLabelText.innerText = "Max population";
  var maxInput = document.createElement("input");
  maxInput.setAttribute("type", "number");
  maxInput.setAttribute("disabled", true);
  maxInput.setAttribute("min", 0);
  maxInput.value = 1e6;

  maxLabel.appendChild(maxLabelText);
  maxLabel.appendChild(maxInput);

  this.container.appendChild(maxLabel);

  this.updateRange = function () {
    var minValue = parseInt(minInput.value);
    var maxValue = parseInt(maxInput.value);

    minInput.setAttribute("max", maxValue - 1);
    maxInput.setAttribute("min", minValue + 1);
  }

  this.fireEvent = function () {
    var minValue = parseInt(minInput.value);
    var maxValue = parseInt(maxInput.value);

    if (isNaN(minValue)) return;
    if (isNaN(maxValue)) return;

    if (minValue < maxValue) {
      eventBus.fire(events.POPULATION_FILTERING, {
        enabled: enableCheckbox.checked,
        minValue: minValue,
        maxValue: maxValue
      });
    }
  }

  enableCheckbox.addEventListener("change", function () {
    if (enableCheckbox.checked) {
      minInput.removeAttribute("disabled");
      maxInput.removeAttribute("disabled");
    } else {
      minInput.setAttribute("disabled", true);
      maxInput.setAttribute("disabled", true);
    }

    this.fireEvent();
  }.bind(this));

  // TODO: validate range, notify user
  minInput.addEventListener("input", function () {
    this.updateRange();
    this.fireEvent();
  }.bind(this));

  maxInput.addEventListener("input", function () {
    this.updateRange();
    this.fireEvent();
  }.bind(this));

  this.updateRange();
  node.appendChild(this.container);
}

module.exports = FilterData;
