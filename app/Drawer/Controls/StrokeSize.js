var events = require('EventBus/Events.js');
var Slider = require('./Slider.js');
require("./PointSize.css");

function PointSize (parent, eventBus) {
  this.container = document.createElement("div");
  this.container.className = "borderSize controlGroup";
  
  var slider = new Slider(this.container, 0, 10, 0, "Border size", true);
  slider.onValueChange = function (value) {
    eventBus.fire(events.STROKE_SIZE_UPDATE, value);
  }

  parent.appendChild(this.container);
}

module.exports = PointSize;
