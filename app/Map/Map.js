var L = require("leaflet");
var events = require('EventBus/Events.js');
var cv = require('./cv.js');
require("leaflet/dist/leaflet.css");

// Europe, sort of
var BASE_COORD = [50, 20];
var BASE_ZOOM = 5;

// 'Cloropleth' map settings
var MIN_LIGHTNESS = 30;
var MAX_LIGHTNESS = 50;

// When using proportional size, the minimum size is the selected size divided by this value
var SIZE_RATIO = 10;

// show all the points
var INSANE = true;
var SANE_AMOUNT = 100;

function Map (mapId, eventBus, styleManager) {
  this.renderOptions = {
    relativeSize: false,
    relativeFill: false,
    minPopulation: 0,
    maxPopulation: 0,
    filterPopulation: false,
    popMinFilter: -Infinity,
    popMaxFilter: Infinity,
  };

  this.style = function (feat) {
    var style = styleManager.style;
    var pop = feat.properties.pop_max;
    
    // This is wrong-ish, because it assummes the min is 0
    var ratio = pop / this.renderOptions.maxPopulation;
    // This creates a 0.1 precision scale adjustment. 0 < x < 0.2 => 0.1 & so on
    // Uncomment for more consistent points
    // ratio = Math.ceil(ratio * 10) / 10;

    if (this.renderOptions.relativeSize) {
      style.radius = (ratio * (style.baseRadius - (style.baseRadius / SIZE_RATIO))) + style.baseRadius / SIZE_RATIO;
    } else {
      style.radius = style.baseRadius;
    }

    if (this.renderOptions.relativeFill) {
      style.fillColor = style.baseFillColor.lightness(((1 - ratio) * (MAX_LIGHTNESS - MIN_LIGHTNESS)) + MIN_LIGHTNESS);
    } else {
      style.fillColor = style.baseFillColor.toString();
    }

    return style;
  }.bind(this);

  this.pointToLayer = function (feature, latlng) {
    // If we use L.circle the zoom behaviour is different, because it uses meters as unit,
    // and circleMarkers uses pixels.
    // We could switch to circles, but we'd have to change the scale of the inputs.
    return L.circleMarker(latlng).bindTooltip(feature.properties.name + " (" + feature.properties.pop_max + ")");
  }

  this.node = null;
  
  this.map = L.map(mapId).setView(BASE_COORD, BASE_ZOOM);
  var options = {};
  options.style = this.style;
  options.pointToLayer = this.pointToLayer;
  this.dataLayer = L.geoJSON(null, options).addTo(this.map);
  this.otherLayer = L.geoJSON(cv, {
    pointToLayer: function (feature, latlng) {
      return L.marker(latlng, {
        icon: L.icon({ iconUrl: "https://pbs.twimg.com/profile_images/800030114906079232/blGBzZTH_bigger.jpg", iconSize: [32, 32] })
      }).bindTooltip(feature.properties.desc);
    }
  });

  L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZHJyb3VtYW4iLCJhIjoiY2oxaG5iOGl0MDBiZzJ3cG02a2pic3JsYSJ9.fhoR-xdGZA4XZBXWHToRHA', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    minZoom: 3,
  }).addTo(this.map);

  this.setGeoJSON = function (geoJSON, extra) {
    // This is for development purposes only
    var data = {
      features: INSANE ? geoJSON.features : geoJSON.features.slice(0, SANE_AMOUNT),
      type: geoJSON.type
    };

    // Calculate min & max population values
    var extra = data.features.reduce(function (acc, feature, index) {
      var population = feature.properties.pop_max;
      
      if (population > acc.max) {
        acc.max = population;
        acc.iMax = index;
      }

      if (acc.min === -1 ||
          (population < acc.min &&
            population > 0)) {
        acc.min = population;
        acc.iMin = index;
      }

      return acc;
    }, { min: -1, iMin: -1, max: -1, iMax: -1 });

    this.renderOptions.minPopulation = extra.min;
    this.renderOptions.maxPopulation = extra.max;

    this.dataLayer.addData(data);
  }

  this.refreshData = function () {
    this.dataLayer.eachLayer(function (layer) {
      var feature = layer.feature;
      var pop = feature.properties.pop_max;

      // My way of 'filtering' data, Leaflet has no layer.hide, so I am improvising here
      if (this.renderOptions.filterPopulation &&
          (pop < this.renderOptions.popMinFilter || pop > this.renderOptions.popMaxFilter)) {
        // I tried using a 0 radius but setStyle safe checks radius, which ignores 0 (not sure if bug or feature =_=)
        layer.getElement().style.display = "none";
      } else {
        layer.getElement().style.display = null;
        layer.setStyle(this.style(feature));
      }
    }, this);
  }

  eventBus.addHandler(events.STYLE_UPDATED, this.refreshData, this);
  eventBus.addHandler(events.SIZE_RELATIVE_POPULATION, function (relative) {
    this.renderOptions.relativeSize = relative;
    this.refreshData();
  }, this);
  eventBus.addHandler(events.FILL_RELATIVE_POPULATION, function (relative) {
    this.renderOptions.relativeFill = relative;
    this.refreshData();
  }, this);
  eventBus.addHandler(events.POPULATION_FILTERING, function (e) {
    this.renderOptions.filterPopulation = e.enabled;
    this.renderOptions.popMinFilter = e.minValue;
    this.renderOptions.popMaxFilter = e.maxValue;
    this.refreshData();
  }, this)
  eventBus.addHandler(events.KONAMI_CODE, function () {
    // Too tired to come up with something more creative, sorry
    this.otherLayer.addTo(this.map);
  }, this);
}

module.exports = Map;
