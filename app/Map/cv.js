module.exports = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-4.787946939468384, 37.895726139627094],
      },
      properties: {
        desc: "May 2011 - Nov 2012 - Serendipio - Fullstack Developer"
      }
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [9.643249511718752, 47.308263132341494],
      },
      properties: {
        desc: "Nov 2012 - Nov 2015 - Omicron Electronics - Web developer"
      }
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-4.729346036911012, 37.907933741395006],
      },
      properties: {
        desc: "Feb 2016 - Nov 2016 - ICCA - Software Engineer"
      }
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-4.782335758209229, 37.88419817175066],
      },
      properties: {
        desc: "Nov 2016 - Apr 2017 - Audiense - Frontend Engineer"
      }
    }
]};
