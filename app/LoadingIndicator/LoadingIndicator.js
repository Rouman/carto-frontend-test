require('./LoadingIndicator.css');

function LoadingIndicator (node) {
  this.node = node;
  this.textNode = node.querySelector("span");

  this.hide = function () {
    this.node.className = "fadeout";
  };

  this.setProgress = function (progress) {
    this.textNode.innerText = "Loading GeoJSON (" + progress + ")";
  };
};

module.exports = LoadingIndicator;