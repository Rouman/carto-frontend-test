var KILOBYTE = 1024;
var MEGABYTE = KILOBYTE * KILOBYTE;

module.exports = function (size) {
  if (size < KILOBYTE) {
    return size + " bytes";
  }

  if (size > KILOBYTE && size < MEGABYTE) {
    return (size / KILOBYTE).toFixed(1) + " KiB";
  }

  return (size / MEGABYTE).toFixed(1) + " MiB";
}
