require('../css/main.css');
var EventBus = require('EventBus/EventBus.js');
var events = require('EventBus/Events.js');
var StyleManager = require('StyleManager/StyleManager.js');
var unitForSize = require('unitForSize.js');
var LoadingIndicator = require('LoadingIndicator/LoadingIndicator.js');
var Map = require('Map/Map.js');
var Drawer = require('Drawer/Drawer.js');

var mapData;

var URL = "api/data.geojson";
URL = "https://xavijam.carto.com/api/v2/sql?q=SELECT%20*%20FROM%20ne_10m_populated_places_simple&format=GeoJSON";

function App () {
  this.mapData = null;
  this.loadingIndicator = null;
  this.drawer = null;
  this.eventBus = new EventBus();
  this.styleManager = new StyleManager(this.eventBus);

  this.init = function () {
    console.log("%cLet's go", "background: #333; color: #bafa66");
    this.loadingIndicator = new LoadingIndicator(document.querySelector("#loading"));
    this.map = new Map("map", this.eventBus, this.styleManager);
    this.drawer = new Drawer(document.querySelector("#drawer"), this.eventBus);
    this.fetchData();
  };

  this.dataLoaded = function (data) {
    this.loadingIndicator.hide();
    mapData = JSON.parse(data.target.response);
    this.map.setGeoJSON(mapData);
  };

  this.dataProgress = function (data) {
    if (data.total) {
      this.reportProgress(Math.round((data.loaded/data.total) * 100) + "%");
    } else {
      this.reportProgress(unitForSize(data.loaded));
    }
  };

  this.reportProgress = function (progress) {
    this.loadingIndicator.setProgress(progress);
  };

  this.fetchData = function () {
    var req = new XMLHttpRequest();

    // TODO: error handling
    req.addEventListener("load", this.dataLoaded.bind(this));
    req.addEventListener("progress", this.dataProgress.bind(this));
    req.open("GET", URL);
    req.send();
  }

  this.eggProgress = 0;
  this.targetkeys = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
  document.addEventListener("keyup", function (e) {
    if (e.keyCode === this.targetkeys[this.eggProgress]) {
      this.eggProgress++;
    } else {
      this.eggProgress = 0;
    }

    if (this.eggProgress === this.targetkeys.length) {
      this.eventBus.fire(events.KONAMI_CODE);
    }
  }.bind(this));
};

var app = new App();
document.addEventListener("DOMContentLoaded", function () {
  app.init();
});
