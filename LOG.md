This turned out very long, sorry about that.

I'll use this file as a sort of log of the mental process.

Gone with webpack just because the dev server is quite easy to setup. It makes CSS modularization very easy too.

The loading indicator is there to test the environment with a simple component.

During development I'll kept a local copy of the API response just to be able to work offline, I'll keep it in the repo but I will also include something to switch to the production endpoint.

I am likely going to use Leaflet for the map, I have some experience with it. Haven't decided whether to use npm or just put the CDN version. Probably the first.

Leaflet is working with mapbox fine. You have to make geoJSON points into CircleMarkers or the page will freeze. Also, I knew this beforehand, but it sets some ridiculous z-indexes, so excuse my measures against those.

Got a layout that should work everywhere, using basic flex. Drawer can open and close, will contain all controls & be responsible for instantiating them.

Thinking about the way to organize the event handling / information flow. 
I am going to keep the style on a StyleManager so the Map can read it and force the update on the data layer.

Got a couple of ideas:
- Pass StyleManager to all controls, they each call setters that update the style & it notifies the map on style changes.
- Writing a basic event bus. UI controls will write events (UPDATE_COLOR, color) to it, the StyleManager will consume them. It will update itself and emit an event that will trigger an update on the map.

The first one is straightforward, but increases coupling.

The second one is a bit more annoying to implement, but decouples controls from the rest of the world, and we could use it for other stuff.

I'll go for the second one, it's more my style. I've created an Events.js to avoid magic strings all around

At this point I've improved webpack resolve, so it now looks for modules on app/ if it doesn't find it on node_modules. I've also just realized that I could be using import syntax:

`import { UPDATE_COLOR } from "EventBus/Events.js"`

The destructuring is very useful to import only what you need, but it's ES2015 so I'll avoid it.

I've tested a button that constantly increases the size of points and it updates on the map, so the update logic circle is closed. I'll start designing controls to style the points on the map now.

Encapsulated a slider using `<input type="range" />`, which should work on all required browsers. The look and feel will vary, I might give it a try to style. Writing my own slider was a bit out of scope, and I think it's more intuitive than text inputs.

Used the slider for a simple reusable color picker, wiring it to change colors around was pretty easy.

Also used the slider to change the size of points, also easy to connect with everything.

I tried switching the shape from a circle to an arbitrary polygon, but it's not as easy as it sounds, they look deformed, probably because of the map projection.

At this point I can't think of much else to style. Everything works pretty well, but looks pretty bad, I might hide the controls on a more "collapsed" view like the screenshot from the test.

Just for fun I added options to make point size & fill color proportional to the pop_max property. For the color I used the alpha initially, but I ended up converting the color to HSL and changing the Lightness (ranging from 30% to 50%).

Also just for fun I added a couple of inputs to 'hide' points based on pop_max. Turns out Leaflet doesn't offer a 'hide' method for layers, so I had to make them transparent.

I have also found some sort of bug with leaflet & the layout, but I can't reproduce it consistently. It doesn't draw geoJSON points from a certain longitude, feels like it thinks that part of the map is not visible. I'll investigate it.

The bug was caused by setting radius as a string, the control is now sending a numerical value to avoid it.